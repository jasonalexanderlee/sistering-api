const Sequelize = require('sequelize');
var db = require('../database');

var relationships= db.define('relationships', {
    id : {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    origin : {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    destination : {
      type: Sequelize.INTEGER,
      allowNull: false
    }
}, {

    timestamps: false,
})

module.exports = relationships;