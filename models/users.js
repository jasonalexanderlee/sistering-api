const Sequelize = require('sequelize');
var db = require('../database');

var users = db.define('users', {
    id : {
      type : Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    username: {
        type : Sequelize.TEXT,
        allowNull: false
    },
    password : {
        type : Sequelize.TEXT,
        allowNull: false
    },
    role : {
        type : Sequelize.TEXT,
        allowNull: false
    },


}, {
    tableName: 'users',
    timestamps: false,
})

module.exports = users;