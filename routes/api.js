var express = require('express');
var router = express.Router();
var fs = require ('fs');
var helpers = require ('./helpers');
var spawn = require('child_process').spawn;
var os = require('os').platform();
var Models = require('../models/models')

var chatsRoutes = require('./controllers/chat');
var relationshipsRoutes = require('./controllers/relationships');
var botconfigRoutes = require('./controllers/botconfig');
var authRoutes = require('./controllers/auth');
var deployRoutes = require('./controllers/deploy');

var token = require('token');
//change later
token.defaults.secret = 'basdaweuh1o123'
token.defaults.timeStep = 7 * 24 * 60 * 60;

token.isValid = function (incoming_token, role = 'admin') {
    return token.verify(role, incoming_token);
}

router = relationshipsRoutes.registerRoutes(router, Models, token);
router = chatsRoutes.registerRoutes(router, Models, token);
router = botconfigRoutes.registerRoutes(router, Models, token);
router = authRoutes.registerRoutes(router, Models, token);
router = deployRoutes.registerRoutes(router, Models, token);

module.exports = router