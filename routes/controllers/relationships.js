module.exports = {

    registerRoutes: (router, models, token) => {
        var relationships = models.relationships;
        var chats = models.chats;
        router.get('/relationships', (req, res) => {
            /*
            if (!token.isValid(req.headers["x-auth"])) {
               return res.status(401).send('unauthorized');
            }
            */
            return relationships.findAll()          
                .then(result =>{    
                    res.status(200).json(result);
                }) 
                
        });

        router.get('/relationships/:id', (req, res) => {
            /*
            if (!token.isValid(req.headers["x-auth"])) {
               return res.status(401).send('unauthorized');
            }
            */
            return relationships.findAll({where : {origin: req.params.id}} )          
                .then((result) =>{    
                    // console.log(req.params)
                    console.log("URL PARAM REQUESTED = "+ req.params.id)
                    var _results = [];
                    // console.log("----------Results------");
                    // console.log(result)
                    // console.log("---------- end Results------");
                    // res.status(200).json(_results);
                    if(result.length == 0){
                        res.status(200).json(_results);
                    }
                    result.forEach(function(val, index, arr){
                         // var val = result[i];

                        console.log("----------For each val------");
                        console.log(val.dataValues)
                        console.log("---------- end For Each End------");
                        chats.find({where: {id: val.dataValues.origin}})
                            .then( (or_results) => {
                                console.log("----------Or res val------");
                                console.log(or_results.dataValues)
                                console.log("---------- OR res  End------");
                                chats.find({where: {id: val.dataValues.destination}})
                                    .then((dest_result) => {
                                        console.log("----------Or res val------");
                                        console.log(dest_result.dataValues)
                                        console.log("---------- OR res  End------");
                                        _results.push({"id": val.dataValues.id,"origin_id":or_results.dataValues.id ,"origin_msg" : or_results.dataValues.message, "destination_id":dest_result.dataValues.id, "destination_msg": dest_result.dataValues.message});
                                        if(arr.length == index+1){
                                            console.log("////----------Final res sent------");
                                            console.log(_results)
                                            console.log("////---------- End Final res sent------");
                                            res.status(200).json(_results);
                                        }
                                        
                                    });
                            });
                        
                    });                 
                    
                }); 
                
        });

        router.delete('/relationships/:id', (req, res) => {
            /*
            if (!token.isValid(req.headers["x-auth"])) {
                return res.status(401).send('unauthorized');
            }
            */
            return relationships.destroy({where: {id: req.params.id}})          
                .then(result =>{    
                    return res.status(200).send('ok');
                }) 
                .catch(error =>{
                    return res.status(500).send('error');
                })
                
        });

        router.post('/relationships/upsert', (req, res) => {
            /*
            if (!token.isValid(req.headers["x-auth"])) {
                return res.status(401).send('unauthorized');
            }
            */
            
            if (!req.body.id){
                return relationships.create(req.body)
                    .then((created) => {
                        return res.status(200).json(created);
                    })
                    .catch((e) => {
                        console.log(req.body)
                        return res.status(500).send('error = '+ e);
                    })
            } else {
                return relationships.update(req.body, { where: { id : req.body.id } })
                    .then(() => {
                        return  res.status(200).json(req.body);
                    })
                    .catch((e) => {
                        return res.status(500).send('error');
                    })
            }

        });



        return router;
    },
    
}