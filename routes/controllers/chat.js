module.exports = {

    registerRoutes: (router, models, token) => {
        
        var chats = models.chats;
        var relationships = models.relationships;       

        router.get('/chats', (req, res) => {
            if (!token.isValid(req.headers["x-auth"])) {
                return res.status(401).send('unauthorized');
            }
            
            return chats.findAll()          
                .then(result =>{    
                    return res.json(result);
                }) 
                .catch(()=>{
                    return res.status(500).send('error') 
                })
                
        });

        router.get('/chats/:id', (req, res) => {
            /*
            if (!token.isValid(req.headers["x-auth"])) {
                return res.status(401).send('unauthorized');
            }
            */
            return chats.find({where:{ id: req.params.id}})      
                .then(result =>{
                    return  res.json(result);
                }) 
                .catch(()=>{
                    return  res.status(500).send('error') 
                })
            
        });

        router.delete('/chats/:id', (req, res) => {
            /*
            if (!token.isValid(req.headers["x-auth"])) {
                return res.status(401).send('unauthorized');
            }
            */
            return chats.destroy({ where: { id: req.params.id } })
                .then (() => {
                   return  relationships.destroy({where: { origin: req.params.id }})
                })
                .then(() => {
                    return relationships.destroy({where: { destination: req.params.id }});
                })
                .then(() =>{
                    return res.status(200).send('ok');
                })
                .catch(()=>{
                    return  res.status(500).send('error') 
                })
        })

        router.post('/chats/upsert', (req, res) => {
            /*
            if (!token.isValid(req.headers["x-auth"])) {
                return res.status(401).send('unauthorized');
            }
            */


            if (!req.body.id){
                return chats.create(req.body)
                .then((resp) =>{
                    return res.json(resp);
                 })
                .catch((err)=>{
                    return  res.status(500).send('error '+err) 
                })
            } else {
                return chats.update(req.body, {where: {id: req.body.id}})     
                    .then((resp) =>{
                        return res.json(req.body);
                    })
                    .catch(()=>{
                        return  res.status(500).send('error') 
                    })
            }  
        });       


        return router;
    }
}