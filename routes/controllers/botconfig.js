module.exports = {

    registerRoutes: (router, models, token) => {

        var botconfig = models.botconfig;

        router.get('/botconfig', (req, res) => {
            if (!token.isValid(req.headers["x-auth"])) {
                return res.status(401).send('unauthorized');
            }
            return botconfig.findAll()          
                .then(result =>{    
                   return res.json(result);
                }) 
                
        });

        router.post('/botconfig', (req, res) => {
            if (!token.isValid(req.headers["x-auth"])) {
                return res.status(401).send('unauthorized');
            }

            let config = req.body;
            config.id = 1;
         
            return botconfig.upsert(config)
                    .then(result => {
                       return res.status(200).json(result);
                    })
                    .catch(error => {
                       return res.status(500).send('error')
                    })              
            });   



        return router;
    }
}